package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IService;
import ru.volkova.tm.entity.User;
import java.util.Optional;


public interface IUserService extends IService<User> {

    @NotNull
    Optional<User> setPassword(@NotNull String userId, @Nullable String password);

}
