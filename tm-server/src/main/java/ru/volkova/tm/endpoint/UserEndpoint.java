package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.endpoint.IUserEndpoint;
import ru.volkova.tm.api.service.IUserService;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.entity.Session;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Optional;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @WebMethod
    public User setPassword(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "password", partName = "password") String password
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getUserService().setPassword(userId, password).orElse(null);
    }

}
