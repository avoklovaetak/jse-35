package ru.volkova.tm.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.api.service.IUserService;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.marker.UnitCategory;
import ru.volkova.tm.repository.UserRepository;
import ru.volkova.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    private final IUserRepository userRepository = new UserRepository();

    private final IPropertyService propertyService = new PropertyService();

    private final IUserService userService = new UserService(userRepository, propertyService);

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final User user = new User();
        Assert.assertNotNull(userService.add(user));
    }

}
