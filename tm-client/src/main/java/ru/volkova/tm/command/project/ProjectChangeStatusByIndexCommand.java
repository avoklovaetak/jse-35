package ru.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Status;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.endpoint.Project;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "change project status by index";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER ID");
        if (bootstrap == null) throw new ObjectNotFoundException();
        if (endpointLocator == null) throw new ObjectNotFoundException();
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Session session = bootstrap.getSession();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusId = TerminalUtil.nextLine();
        @Nullable final Status status = Status.valueOf(statusId);
        endpointLocator.getProjectEndpoint().
                changeProjectStatusByIndex(session, index, status);
    }

    @NotNull
    @Override
    public String name() {
        return "project-change-status-by-index";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
